package org.gameg.service;

import org.gameg.model.SysEmail;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface ISysEmailService extends IBaseService<SysEmail> {

}
