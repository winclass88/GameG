package org.gameg.service;

import org.gameg.model.SysNotice;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface ISysNoticeService extends IBaseService<SysNotice> {

}
