package org.gameg.service;

import org.gameg.model.SysEmailConfig;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface ISysEmailConfigService extends IBaseService<SysEmailConfig>{

}
