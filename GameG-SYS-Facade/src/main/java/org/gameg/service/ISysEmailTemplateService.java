package org.gameg.service;

import org.gameg.model.SysEmailTemplate;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface ISysEmailTemplateService extends IBaseService<SysEmailTemplate> {

}
