package org.gameg.service;

import org.gameg.model.SysArticle;

import top.ibase4j.core.base.IBaseService;

/**
 * 文章  服务
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:43:15
 */
public interface ISysArticleService extends IBaseService<SysArticle> {

}
