package org.gameg.service;

import org.gameg.model.SysNews;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface ISysNewsService extends IBaseService<SysNews> {

}
