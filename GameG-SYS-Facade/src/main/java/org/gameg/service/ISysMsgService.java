package org.gameg.service;

import org.gameg.model.SysMsg;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:59:39
 */
public interface ISysMsgService extends IBaseService<SysMsg> {
    
}
