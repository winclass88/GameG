package org.gameg.service;

import org.gameg.model.SysFeedback;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:55:44
 */
public interface ISysFeedbackService extends IBaseService<SysFeedback> {

}
