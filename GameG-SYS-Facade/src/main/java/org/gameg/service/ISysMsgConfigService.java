package org.gameg.service;

import org.gameg.model.SysMsgConfig;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:58:43
 */
public interface ISysMsgConfigService extends IBaseService<SysMsgConfig> {

}
