package org.gameg.service;

import org.gameg.model.SysDept;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午10:59:30
 */
public interface ISysDeptService extends IBaseService<SysDept> {

}
