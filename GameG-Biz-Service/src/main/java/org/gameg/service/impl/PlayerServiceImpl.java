package org.gameg.service.impl;

import org.gameg.mapper.TPlayerMapper;
import org.gameg.model.TPlayer;
import org.gameg.service.IPlayerService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 玩家  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@CacheConfig(cacheNames = "TPlayer")
@Service(interfaceClass = IPlayerService.class)
@MotanService(interfaceClass = IPlayerService.class)
public class PlayerServiceImpl extends BaseService<TPlayer, TPlayerMapper> implements IPlayerService {
	
}