package org.gameg.service.impl;

import org.gameg.mapper.TGamePowerMapper;
import org.gameg.model.TGamePower;
import org.gameg.service.IGamePowerService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 游戏倍率配置  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@CacheConfig(cacheNames = "TGamePower")
@Service(interfaceClass = IGamePowerService.class)
@MotanService(interfaceClass = IGamePowerService.class)
public class GamePowerServiceImpl extends BaseService<TGamePower, TGamePowerMapper> implements IGamePowerService {

}
