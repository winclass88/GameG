package org.gameg.service.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.gameg.bean.Game;
import org.gameg.bean.Player;
import org.gameg.bean.Power;

import top.ibase4j.core.util.DataUtil;

/**
 * 点数
 * @author ShenHuaJie
 */
public class PointHelper {
    static final Logger logger = LogManager.getLogger();

    public static final String[] g = {"", "牛1", "牛2", "牛3", "牛4", "牛5", "牛6", "牛7", "牛8", "牛9", "牛牛", "金牛", "对子", "顺子",
        "倒顺", "满牛", "豹子"};

    /**
     * 获取点数
     * @param config 配置(dt位数<1:分2:角分3:圆角分>;<br>
     * jnt金牛种类<1代表：0.10,0.20,0.30,0.40到0.90; 2代表：0.10,1.10,2.10,3.10到9.10; 3代表：0.10,1.10>;<br>
     * dzt对子种类<1代表：0.11,0.22,0.33.0.44到0.99; 2代表：0.11,0.22,0.33.0.44...0.99,1.22,1,33..1.99...2.11..2.33..2.99到9.88>;<br>
     * szt顺子种类<1代表：0.12,1.23,2.34,3.45到7.89; 2代表：1.23,2.34,3.45,4.56,到7.89>)
     * @param amount 金额
     * @return 对应规则的比重值(可比较大小)
     */
    public static Integer getPoint(Game config, Map<Integer, Power> powers, BigDecimal amount) {
        String digit = DataUtil.ifNull(config.getDt(), "3");
        String jinniuType = DataUtil.ifNull(config.getJnt(), "2");
        String duiziType = DataUtil.ifNull(config.getDzt(), "2");
        String shunziType = DataUtil.ifNull(config.getSzt(), "2");
        logger.info("参数：取{}位数算牛，第{}种金牛，第{}种对子，第{}种顺子", digit, jinniuType, duiziType, shunziType);
        int a = amount.intValue();
        int b = amount.subtract(new BigDecimal(String.valueOf(a))).multiply(new BigDecimal("10")).intValue();
        int c = amount.multiply(new BigDecimal("100")).remainder(new BigDecimal("10")).intValue();
        Integer result;
        if (powers.get(16).getIsOpen() && a == b && a == c) { // 豹子
            result = powers.get(16).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 16 + "=>牛数：" + g[result]);
        } else if (powers.get(15).getIsOpen() && a != 0 && b == 0 && c == 0) { // 满牛
            result = powers.get(15).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 15 + "=>牛数：" + g[result]);
        } else if (powers.get(14).getIsOpen() && a - b == 1 && b - c == 1 && c != 0) { // 倒顺
            result = powers.get(14).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 14 + "=>牛数：" + g[result]);
        } else if (powers.get(13).getIsOpen() && ("1".equals(shunziType) && c - b == 1 && b - a == 1)
            || ("2".equals(shunziType) && a != 0 && c - b == 1 && b - a == 1)) { // 顺子
            result = powers.get(13).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 13 + "=>牛数：" + g[result]);
        } else if (powers.get(12).getIsOpen()
            && (("1".equals(duiziType) && a == 0 && b == c) || ("2".equals(duiziType) && b == c))) { // 对子
            result = powers.get(12).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 12 + "=>牛数：" + g[result]);
        } else if (powers.get(11).getIsOpen()
            && (("1".equals(jinniuType) && a == 0 && c == 0) || ("2".equals(jinniuType) && b == 1 && c == 0)
                || ("3".equals(jinniuType) && (amount.doubleValue() == 0.10 || amount.doubleValue() == 1.10)))) { // 金牛
            result = powers.get(11).getNiuPoint();
            logger.info("红包：" + amount + "====>点数：" + 11 + "=>牛数：" + g[result]);
        } else {
            int d = -1;
            if ("1".equals(digit)) {
                d = c;
            } else if ("2".equals(digit)) {
                d = b + c;
            } else if ("3".equals(digit)) {
                d = a + b + c;
            } else {
                throw new RuntimeException("计算位数设置错误.");
            }
            if (d > 10) {
                d = d % 10;
            }
            if (d == 0) {
                d = 10;
            }
            result = d;
            logger.info("红包：" + amount + "====>点数：" + d + "=>牛数：" + g[result]);
        }
        return result;
    }

    /** 
     * 比大小
     * point 点数
     * amount 红包金额
     * time 抢红包所用时间
     */
    public static int compare(Player a, Player b) {
        int aPoint = a.getPoint();
        int bPoint = b.getPoint();
        if (aPoint > bPoint) { // 庄赢
            return 1;
        } else if (bPoint > aPoint) { // 闲赢
            return -1;
        } else { // 同点
            // 比金额
            double aAmount = a.getAmount().doubleValue();
            double bAmount = b.getAmount().doubleValue();
            if (aAmount > bAmount) { // 庄赢
                return 1;
            } else if (bAmount > aAmount) { // 闲赢
                return -1;
            } else { // 同点
                // 比时间
                double aTime = a.getTime();
                double bTime = b.getTime();
                if (aTime < bTime) { // 庄赢
                    return 1;
                } else if (bTime < aTime) { // 闲赢
                    return -1;
                } else { // 同点
                    return 0;
                }
            }
        }
    }
}
