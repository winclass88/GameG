package org.gameg.service.impl;

import org.gameg.mapper.TGamePlayerMapper;
import org.gameg.model.TGamePlayer;
import org.gameg.service.IGamePlayerService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 游戏玩家  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@CacheConfig(cacheNames = "TGamePlayer")
@Service(interfaceClass = IGamePlayerService.class)
@MotanService(interfaceClass = IGamePlayerService.class)
public class GamePlayerServiceImpl extends BaseService<TGamePlayer, TGamePlayerMapper> implements IGamePlayerService {

}
