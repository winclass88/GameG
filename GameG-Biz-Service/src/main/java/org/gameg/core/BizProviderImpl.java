package org.gameg.core;

import org.gameg.provider.IBizProvider;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.provider.BaseProviderImpl;

@Service(interfaceClass = IBizProvider.class)
@MotanService(interfaceClass = IBizProvider.class)
public class BizProviderImpl extends BaseProviderImpl implements IBizProvider {

}