package org.gameg.mapper;

import org.gameg.model.TPlayer;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
public interface TPlayerMapper extends BaseMapper<TPlayer> {

}