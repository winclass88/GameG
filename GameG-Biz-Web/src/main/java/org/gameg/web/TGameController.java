package org.gameg.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gameg.bean.Game;
import org.gameg.model.TGame;
import org.gameg.service.IGameService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 游戏  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@Controller
@RequestMapping("/tGame")
@Api(value = "游戏接口", description = "游戏接口")
public class TGameController extends BaseController<TGame, IGameService> {
    @RequiresPermissions("tGame.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询游戏", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("tGame.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "游戏详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TGame param = WebUtil.getParameter(request, TGame.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("tGame.update")
    @ApiOperation(value = "修改游戏", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        Game param = WebUtil.getParameter(request, Game.class);
        service.updateGameResult(param);
        return setSuccessModelMap();
    }

    @DeleteMapping
    @RequiresPermissions("tGame.delete")
    @ApiOperation(value = "删除游戏", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TGame param = WebUtil.getParameter(request, TGame.class);
        return super.del(request, param);
    }
}
