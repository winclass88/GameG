package org.gameg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = {"top.ibase4j", "org.gameg"})
public class SysWebApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(SysWebApplication.class, args);
	}
}