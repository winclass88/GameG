package org.gameg.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import top.ibase4j.core.base.BaseModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 游戏玩家
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@ApiModel("游戏玩家")
@TableName("t_game_player")
@SuppressWarnings("serial")
public class TGamePlayer extends BaseModel {

	@TableField("game_id")
	private Long gameId;
	@TableField("player_id")
	private Long playerId;
    @ApiModelProperty(value = "红包金额")
	@TableField("amount_")
	private BigDecimal amount;
    @ApiModelProperty(value = "牛数")
	@TableField("point_")
	private Integer point;
    @ApiModelProperty(value = "抢红包用时")
	@TableField("time_")
	private Double time;
    @ApiModelProperty(value = "是否庄家")
	@TableField("is_zj")
	private String isZj;
    @ApiModelProperty(value = "输赢积分")
	@TableField("credit_score")
	private Integer creditScore;


	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

	public String getIsZj() {
		return isZj;
	}

	public void setIsZj(String isZj) {
		this.isZj = isZj;
	}

	public Integer getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(Integer creditScore) {
		this.creditScore = creditScore;
	}

}