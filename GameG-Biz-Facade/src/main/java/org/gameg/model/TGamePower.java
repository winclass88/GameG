package org.gameg.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import top.ibase4j.core.base.BaseModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 游戏倍率配置
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@ApiModel("游戏倍率配置")
@TableName("t_game_power")
@SuppressWarnings("serial")
public class TGamePower extends BaseModel {

	@TableField("game_id")
	private Long gameId;
    @ApiModelProperty(value = "点数")
	@TableField("point_")
	private Integer point;
    @ApiModelProperty(value = "倍率")
	@TableField("power_")
	private Integer power;
    @ApiModelProperty(value = "牛数")
	@TableField("niu_point")
	private Integer niuPoint;
    @ApiModelProperty(value = "是否特殊牌型")
	@TableField("is_open")
	private String isOpen;


	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public Integer getNiuPoint() {
		return niuPoint;
	}

	public void setNiuPoint(Integer niuPoint) {
		this.niuPoint = niuPoint;
	}

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}

}