package org.gameg.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import top.ibase4j.core.base.BaseModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 玩家
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@ApiModel("玩家")
@TableName("t_player")
@SuppressWarnings("serial")
public class TPlayer extends BaseModel {

    @ApiModelProperty(value = "姓名")
	@TableField("user_name")
	private String userName;
    @ApiModelProperty(value = "密码")
	@TableField("password_")
	private String password;
    @ApiModelProperty(value = "电话")
	@TableField("phone_")
	private String phone;
    @ApiModelProperty(value = "昵称")
	@TableField("nick_name")
	private String nickName;
    @ApiModelProperty(value = "邮箱")
	@TableField("email_")
	private String email;
    @ApiModelProperty(value = "性别(0:未知;1:男;2:女)")
	@TableField("sex_")
	private Integer sex;
    @ApiModelProperty(value = "头像")
	@TableField("avatar_")
	private String avatar;
    @ApiModelProperty(value = "二维码")
	@TableField("qr_code")
	private String qrCode;
    @ApiModelProperty(value = "个性签名")
	@TableField("personal_sign")
	private String personalSign;
    @ApiModelProperty(value = "身份证号码")
	@TableField("id_card")
	private String idCard;
    @ApiModelProperty(value = "出生日期")
	@TableField("birth_day")
	private Date birthDay;
    @ApiModelProperty(value = "微信")
	@TableField("wei_xin")
	private String weiXin;
    @ApiModelProperty(value = "微博")
	@TableField("wei_bo")
	private String weiBo;
    @ApiModelProperty(value = "QQ")
	@TableField("qq_")
	private String qq;
    @ApiModelProperty(value = "APP会话token")
	@TableField("token_")
	private String token;
    @ApiModelProperty(value = "客户端唯一标识")
	@TableField("uuid_")
	private String uuid;
    @ApiModelProperty(value = "微信唯一标识")
	@TableField("wx_id")
	private String wxId;
    @ApiModelProperty(value = "积分")
	@TableField("credit_score")
	private Integer creditScore;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getPersonalSign() {
		return personalSign;
	}

	public void setPersonalSign(String personalSign) {
		this.personalSign = personalSign;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getWeiXin() {
		return weiXin;
	}

	public void setWeiXin(String weiXin) {
		this.weiXin = weiXin;
	}

	public String getWeiBo() {
		return weiBo;
	}

	public void setWeiBo(String weiBo) {
		this.weiBo = weiBo;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public Integer getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(Integer creditScore) {
		this.creditScore = creditScore;
	}

}