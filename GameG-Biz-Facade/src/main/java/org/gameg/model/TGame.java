package org.gameg.model;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import top.ibase4j.core.base.BaseModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 游戏
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-04-21
 */
@ApiModel("游戏")
@TableName("t_game")
@SuppressWarnings("serial")
public class TGame extends BaseModel {

    @ApiModelProperty(value = "牌型（牛牛牌型，百家乐牌型，大小单双合牌型，特码牌型）")
	private String gmp;
    @ApiModelProperty(value = "最高下注用户积分%")
	@TableField("max_user_source")
	private BigDecimal maxUserSource;
    @ApiModelProperty(value = "最高下注庄积分%")
	@TableField("max_banker_source")
	private BigDecimal maxBankerSource;
    @ApiModelProperty(value = "最低下注")
	@TableField("min_bet")
	private Integer minBet;
    @ApiModelProperty(value = "最高下注")
	@TableField("max_bet")
	private Integer maxBet;
    @ApiModelProperty(value = "玩法格式：三位/两位/一位")
	private String dt;
    @ApiModelProperty(value = "金牛种类")
	private String jnt;
    @ApiModelProperty(value = "对子种类")
	private String dzt;
    @ApiModelProperty(value = "顺子种类")
	private String szt;


	public String getGmp() {
		return gmp;
	}

	public void setGmp(String gmp) {
		this.gmp = gmp;
	}

	public BigDecimal getMaxUserSource() {
		return maxUserSource;
	}

	public void setMaxUserSource(BigDecimal maxUserSource) {
		this.maxUserSource = maxUserSource;
	}

	public BigDecimal getMaxBankerSource() {
		return maxBankerSource;
	}

	public void setMaxBankerSource(BigDecimal maxBankerSource) {
		this.maxBankerSource = maxBankerSource;
	}

	public Integer getMinBet() {
		return minBet;
	}

	public void setMinBet(Integer minBet) {
		this.minBet = minBet;
	}

	public Integer getMaxBet() {
		return maxBet;
	}

	public void setMaxBet(Integer maxBet) {
		this.maxBet = maxBet;
	}

	public String getDt() {
		return dt;
	}

	public void setDt(String dt) {
		this.dt = dt;
	}

	public String getJnt() {
		return jnt;
	}

	public void setJnt(String jnt) {
		this.jnt = jnt;
	}

	public String getDzt() {
		return dzt;
	}

	public void setDzt(String dzt) {
		this.dzt = dzt;
	}

	public String getSzt() {
		return szt;
	}

	public void setSzt(String szt) {
		this.szt = szt;
	}

}