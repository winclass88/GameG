package org.gameg.bean;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
public class Game implements Serializable {
    @ApiModelProperty("是否有庄家<Y/N>")
    private String hdZJ = "Y";
    @ApiModelProperty("是否仅比大小<Y/N>")
    private String onlyPoint = "Y";
    @ApiModelProperty("牌型<1牛牛牌型，2百家乐牌型，3大小单双合牌型，4特码牌型>")
    private String gmp = "3";
    @ApiModelProperty("位数<1:分2:角分3:圆角分>")
    private String dt = "3";
    @ApiModelProperty("金牛种类<1代表：0.10,0.20,0.30,0.40到0.90; 2代表：0.10,1.10,2.10,3.10到9.10; 3代表：0.10,1.10>")
    private String jnt = "2";
    @ApiModelProperty("<1代表：0.11,0.22,0.33.0.44到0.99; 2代表：0.11,0.22,0.33.0.44...0.99,1.22,1,33..1.99...2.11..2.33..2.99到9.88>")
    private String dzt = "3";
    @ApiModelProperty("顺子种类<1代表：0.12,1.23,2.34,3.45到7.89; 2代表：1.23,2.34,3.45,4.56,到7.89>")
    private String szt = "3";
    @ApiModelProperty("倍率配置")
    private List<Power> powers;
    @ApiModelProperty("玩家")
    private List<Player> players;

    public String getHdZJ() {
        return hdZJ;
    }

    public void setHdZJ(String hdZJ) {
        this.hdZJ = hdZJ;
    }

    public String getOnlyPoint() {
        return onlyPoint;
    }

    public void setOnlyPoint(String onlyPoint) {
        this.onlyPoint = onlyPoint;
    }

    public String getGmp() {
        return gmp;
    }

    public void setGmp(String gmp) {
        this.gmp = gmp;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getJnt() {
        return jnt;
    }

    public void setJnt(String jnt) {
        this.jnt = jnt;
    }

    public String getDzt() {
        return dzt;
    }

    public void setDzt(String dzt) {
        this.dzt = dzt;
    }

    public String getSzt() {
        return szt;
    }

    public void setSzt(String szt) {
        this.szt = szt;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Power> getPowers() {
        return powers;
    }

    public void setPowers(List<Power> powers) {
        this.powers = powers;
    }
}
