package org.gameg.bean;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
public class Power implements Serializable {

    @ApiModelProperty("点数(1->16)")
    private Integer point;
    @ApiModelProperty("倍率")
    private Integer power;
    @ApiModelProperty("牛数")
    private Integer niuPoint;
    @ApiModelProperty("是否开启特殊排序")
    private Boolean isOpen;

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getNiuPoint() {
        return niuPoint;
    }

    public void setNiuPoint(Integer niuPoint) {
        this.niuPoint = niuPoint;
    }

    public Boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }
}
