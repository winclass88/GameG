package org.gameg.bean;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 会员
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-10-12
 */
@SuppressWarnings("serial")
public class Player extends BaseModel {

    @ApiModelProperty("游戏Id")
    private Long gameId;
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("微信")
    private String wxId;

    @ApiModelProperty("是否庄家")
    private String isZj;
    @ApiModelProperty("红包金额")
    private BigDecimal amount;
    @ApiModelProperty("红包时间")
    private Double time;
    @ApiModelProperty("点数")
    private Integer point;
    @ApiModelProperty("押注")
    private Integer bet;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getIsZj() {
        return isZj;
    }

    public void setIsZj(String isZj) {
        this.isZj = isZj;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getBet() {
        return bet;
    }

    public void setBet(Integer bet) {
        this.bet = bet;
    }
}
