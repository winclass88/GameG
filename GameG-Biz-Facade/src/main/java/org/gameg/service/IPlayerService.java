package org.gameg.service;

import org.gameg.model.TPlayer;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 下午4:07:35
 */
public interface IPlayerService extends IBaseService<TPlayer> {

}
