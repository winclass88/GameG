package org.gameg.service;

import org.gameg.model.TGamePower;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 下午4:02:52
 */
public interface IGamePowerService extends IBaseService<TGamePower> {

}
