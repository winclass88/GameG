package org.gameg.service;

import java.util.List;

import org.gameg.bean.Game;
import org.gameg.model.TGame;
import org.gameg.model.TGamePlayer;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 下午4:06:26
 */
public interface IGameService extends IBaseService<TGame>{
    public List<TGamePlayer> updateGameResult(Game game);
}
