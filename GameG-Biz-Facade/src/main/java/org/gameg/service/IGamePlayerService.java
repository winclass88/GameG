package org.gameg.service;

import org.gameg.model.TGamePlayer;

import top.ibase4j.core.base.IBaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 下午4:01:27
 */
public interface IGamePlayerService extends IBaseService<TGamePlayer>{

}
