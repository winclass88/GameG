package org.gameg.service.impl;

import org.gameg.mapper.SysEmailTemplateMapper;
import org.gameg.model.SysEmailTemplate;
import org.gameg.service.ISysEmailTemplateService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysEmailTemplate")
@Service(interfaceClass = ISysEmailTemplateService.class)
@MotanService(interfaceClass = ISysEmailTemplateService.class)
public class SysEmailTemplateServiceImpl extends BaseService<SysEmailTemplate, SysEmailTemplateMapper>
    implements ISysEmailTemplateService {

}
