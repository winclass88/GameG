package org.gameg.service.impl;

import org.gameg.mapper.SysFeedbackMapper;
import org.gameg.model.SysFeedback;
import org.gameg.service.ISysFeedbackService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 反馈  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "SysFeedback")
@Service(interfaceClass = ISysFeedbackService.class)
@MotanService(interfaceClass = ISysFeedbackService.class)
public class SysFeedbackServiceImpl extends BaseService<SysFeedback, SysFeedbackMapper> implements ISysFeedbackService {

}
