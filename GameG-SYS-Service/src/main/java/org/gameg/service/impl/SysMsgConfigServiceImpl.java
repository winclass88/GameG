package org.gameg.service.impl;

import org.gameg.mapper.SysMsgConfigMapper;
import org.gameg.model.SysMsgConfig;
import org.gameg.service.ISysMsgConfigService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "sysMsgConfig")
@Service(interfaceClass = ISysMsgConfigService.class)
@MotanService(interfaceClass = ISysMsgConfigService.class)
public class SysMsgConfigServiceImpl extends BaseService<SysMsgConfig, SysMsgConfigMapper>
    implements ISysMsgConfigService {

}
