package org.gameg.service.impl;

import org.gameg.mapper.SysUnitMapper;
import org.gameg.model.SysUnit;
import org.gameg.service.ISysUnitService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysUnit")
@Service(interfaceClass = ISysUnitService.class)
@MotanService(interfaceClass = ISysUnitService.class)
public class SysUnitServiceImpl extends BaseService<SysUnit, SysUnitMapper> implements ISysUnitService {

}
