package org.gameg.service.impl;

import org.gameg.mapper.SysEmailConfigMapper;
import org.gameg.model.SysEmailConfig;
import org.gameg.service.ISysEmailConfigService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysEmailConfig")
@Service(interfaceClass = ISysEmailConfigService.class)
@MotanService(interfaceClass = ISysEmailConfigService.class)
public class SysEmailConfigServiceImpl extends BaseService<SysEmailConfig, SysEmailConfigMapper>
    implements ISysEmailConfigService {

}
