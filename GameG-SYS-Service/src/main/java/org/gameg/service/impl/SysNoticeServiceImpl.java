package org.gameg.service.impl;

import org.gameg.mapper.SysNoticeMapper;
import org.gameg.model.SysNotice;
import org.gameg.service.ISysNoticeService;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.weibo.api.motan.config.springsupport.annotation.MotanService;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysNotice")
@Service(interfaceClass = ISysNoticeService.class)
@MotanService(interfaceClass = ISysNoticeService.class)
public class SysNoticeServiceImpl extends BaseService<SysNotice, SysNoticeMapper> implements ISysNoticeService {

}
