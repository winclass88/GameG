package org.gameg.mapper;

import org.gameg.model.SysDic;

import top.ibase4j.core.base.BaseMapper;

/**
 * @author ShenHuaJie
 * @since 2018年3月3日 下午7:22:18
 */
public interface SysDicMapper extends BaseMapper<SysDic> {
}