package org.gameg.mapper;

import org.gameg.model.SysEmail;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-01-29
 */
public interface SysEmailMapper extends BaseMapper<SysEmail> {

}