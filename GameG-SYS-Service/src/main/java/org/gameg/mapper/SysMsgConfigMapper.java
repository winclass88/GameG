package org.gameg.mapper;

import org.gameg.model.SysMsgConfig;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
public interface SysMsgConfigMapper extends BaseMapper<SysMsgConfig> {

}